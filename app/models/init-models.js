var DataTypes = require("sequelize").DataTypes;
var _actions = require("./actions");
var _archive = require("./archive");
var _bulk_logs = require("./bulk_logs");
var _contents = require("./contents");
var _event_payment_methods = require("./event_payment_methods");
var _events = require("./events");
var _invoice_items = require("./invoice_items");
var _invoices = require("./invoices");
var _mail_templates = require("./mail_templates");
var _mails = require("./mails");
var _orders = require("./orders");
var _password_reset = require("./password_reset");
var _payment_methods = require("./payment_methods");
var _payment_status_logs = require("./payment_status_logs");
var _payments = require("./payments");
var _phinxlog = require("./phinxlog");
var _pricing_rules = require("./pricing_rules");
var _ticket_assign_request_tickets = require("./ticket_assign_request_tickets");
var _ticket_assign_requests = require("./ticket_assign_requests");
var _ticket_types = require("./ticket_types");
var _tickets = require("./tickets");
var _timezones = require("./timezones");
var _user = require("./user");
var _user_event_promoters = require("./user_event_promoters");
var _users = require("./users");

function initModels(sequelize) {
  var actions = _actions(sequelize, DataTypes);
  var archive = _archive(sequelize, DataTypes);
  var bulk_logs = _bulk_logs(sequelize, DataTypes);
  var contents = _contents(sequelize, DataTypes);
  var event_payment_methods = _event_payment_methods(sequelize, DataTypes);
  var events = _events(sequelize, DataTypes);
  var invoice_items = _invoice_items(sequelize, DataTypes);
  var invoices = _invoices(sequelize, DataTypes);
  var mail_templates = _mail_templates(sequelize, DataTypes);
  var mails = _mails(sequelize, DataTypes);
  var orders = _orders(sequelize, DataTypes);
  var password_reset = _password_reset(sequelize, DataTypes);
  var payment_methods = _payment_methods(sequelize, DataTypes);
  var payment_status_logs = _payment_status_logs(sequelize, DataTypes);
  var payments = _payments(sequelize, DataTypes);
  var phinxlog = _phinxlog(sequelize, DataTypes);
  var pricing_rules = _pricing_rules(sequelize, DataTypes);
  var ticket_assign_request_tickets = _ticket_assign_request_tickets(sequelize, DataTypes);
  var ticket_assign_requests = _ticket_assign_requests(sequelize, DataTypes);
  var ticket_types = _ticket_types(sequelize, DataTypes);
  var tickets = _tickets(sequelize, DataTypes);
  var timezones = _timezones(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);
  var user_event_promoters = _user_event_promoters(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);

  events.belongsTo(users, { foreignKey: "manager_id"});
  users.hasMany(events, { foreignKey: "manager_id"});
  invoice_items.belongsTo(invoices, { foreignKey: "invoice_id"});
  invoices.hasMany(invoice_items, { foreignKey: "invoice_id"});
  invoices.belongsTo(orders, { foreignKey: "order_id"});
  orders.hasMany(invoices, { foreignKey: "order_id"});
  mails.belongsTo(mail_templates, { foreignKey: "template_id"});
  mail_templates.hasMany(mails, { foreignKey: "template_id"});
  mails.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(mails, { foreignKey: "user_id"});
  orders.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(orders, { foreignKey: "user_id"});
  orders.belongsTo(events, { foreignKey: "event_id"});
  events.hasMany(orders, { foreignKey: "event_id"});
  payments.belongsTo(invoices, { foreignKey: "invoice_id"});
  invoices.hasMany(payments, { foreignKey: "invoice_id"});
  pricing_rules.belongsTo(ticket_types, { foreignKey: "ticket_type_id"});
  ticket_types.hasMany(pricing_rules, { foreignKey: "ticket_type_id"});
  ticket_types.belongsTo(events, { foreignKey: "event_id"});
  events.hasMany(ticket_types, { foreignKey: "event_id"});
  tickets.belongsTo(users, { foreignKey: "manager_id"});
  users.hasMany(tickets, { foreignKey: "manager_id"});
  tickets.belongsTo(events, { foreignKey: "event_id"});
  events.hasMany(tickets, { foreignKey: "event_id"});
  tickets.belongsTo(orders, { foreignKey: "order_id"});
  orders.hasMany(tickets, { foreignKey: "order_id"});
  tickets.belongsTo(ticket_types, { foreignKey: "ticket_type_id"});
  ticket_types.hasMany(tickets, { foreignKey: "ticket_type_id"});
  users.belongsTo(timezones, { foreignKey: "timezone_id"});
  timezones.hasMany(users, { foreignKey: "timezone_id"});

  return {
    actions,
    archive,
    bulk_logs,
    contents,
    event_payment_methods,
    events,
    invoice_items,
    invoices,
    mail_templates,
    mails,
    orders,
    password_reset,
    payment_methods,
    payment_status_logs,
    payments,
    phinxlog,
    pricing_rules,
    ticket_assign_request_tickets,
    ticket_assign_requests,
    ticket_types,
    tickets,
    timezones,
    user,
    user_event_promoters,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
