const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('payments', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true
    },
    public_id: {
      type: DataTypes.CHAR(32),
      allowNull: true
    },
    invoice_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      references: {
        model: 'invoices',
        key: 'id'
      }
    },
    payment_method_id: {
      type: DataTypes.CHAR(36),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "open"
    },
    amount_paid_in_cents: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    billing_cycle: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    payment_method: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    contract_id: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    webhook_url: {
      type: DataTypes.CHAR(255),
      allowNull: true
    },
    checkout_id: {
      type: DataTypes.CHAR(255),
      allowNull: true
    },
    checkout_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mode: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    paid_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    canceled_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    expires_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    failed_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'payments',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "invoice_id",
        using: "BTREE",
        fields: [
          { name: "invoice_id" },
        ]
      },
    ]
  });
};
