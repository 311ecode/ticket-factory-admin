const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('timezones', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    offset_utc: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: "+00:00"
    },
    offset_utc_dst: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: "+00:00"
    }
  }, {
    sequelize,
    tableName: 'timezones',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
