const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true
    },
    firstname: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    lastname: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: "email"
    },
    country: {
      type: DataTypes.STRING(2),
      allowNull: true,
      defaultValue: ""
    },
    sf_username: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: "inactive"
    },
    role: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: "visitor"
    },
    ip: {
      type: DataTypes.STRING(15),
      allowNull: true,
      defaultValue: ""
    },
    config: {
      type: DataTypes.JSON,
      allowNull: true
    },
    last_seen: {
      type: DataTypes.DATE,
      allowNull: true
    },
    timezone_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      references: {
        model: 'timezones',
        key: 'id'
      }
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'users',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "email",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "email" },
        ]
      },
      {
        name: "timezone_id",
        using: "BTREE",
        fields: [
          { name: "timezone_id" },
        ]
      },
    ]
  });
};
