require('cowlog')()
const AdminBro = require('admin-bro')
const AdminBroSequelize = require('@admin-bro/sequelize')
AdminBro.registerAdapter(AdminBroSequelize)
const AdminBroExpress = require('@admin-bro/express')

const express = require('express')
const app = express()

const rootPath = '/'
const adminBro = new AdminBro({
  databases: [require('../app/models')],
  // databases: [],
  rootPath
})

// AdminBro.registerAdapter(AdminBroDb)

const router = AdminBroExpress.buildRouter(adminBro)

app.use(adminBro.options.rootPath, router)
let port = 8084
app.listen(port, () => console.log(`AdminBro is under localhost:${port}${rootPath}`))
